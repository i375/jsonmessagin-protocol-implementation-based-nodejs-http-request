import * as http from "http"
import * as methods from "./methods"
import * as messageHandlers from "./messagehandlers"
import * as types from "./messagingtypes"

type JSONMessageString = string

function buildMessageRoutes(): types.MessageRoutesArray {
    var messageRoutes: types.MessageRoutesArray = new Array()

    messageRoutes[methods.Methods.Login] = messageHandlers.login
    messageRoutes[methods.Methods.DoSomeAction] = messageHandlers.doSomeAction

    return messageRoutes
}

function messageRouter(messageRoutes: types.MessageRoutesArray, data: JSONMessageString, res: http.ServerResponse) {
    const requestJSON:JSONMessage = (() => {
        try{
            return <JSONMessage>JSON.parse(data)
        }catch(e)
        {
            return null
        }
    }
    )()

    

    if (requestJSON && requestJSON.method && messageRoutes[requestJSON.method]) {
        messageRoutes[requestJSON.method](requestJSON, res)
    }
    else {
        res.setHeader("http-code", "500")
        res.end()
    }
}

function main() {

    const messageRoutes = buildMessageRoutes()

    const srv = http.createServer((req, res) => {
        req.on("data", requestDataChunkHandler)
        req.on("end", requestDataChunkEndHandler)

        var data: string = ""

        function requestDataChunkHandler(chunk: string | Buffer) {
            data += chunk
        }

        function requestDataChunkEndHandler() {
            console.log(data)

            messageRouter(messageRoutes, data, res)
        }
    })



    srv.listen(8090)

    console.log("listening on 8090")

}


main()