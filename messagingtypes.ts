/// <reference path="standardsandprotocols.jsonmessaging/JSONMessaging.ts" />

import * as http from "http"

export interface MessageHandler
{
    (requestMessageData:JSONMessage, response:http.ServerResponse):void
}

export interface MessageRoutesArray
{
    [index:number]:MessageHandler
}
