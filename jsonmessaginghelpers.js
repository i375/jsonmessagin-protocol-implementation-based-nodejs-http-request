/// <reference path="standardsandprotocols.jsonmessaging/JSONMessaging.ts" />
"use strict";
function createJSONMEssage(methodId, params, messageId, errorCode) {
    var jsonMessage = {
        method: methodId,
        params: params,
        messageId: messageId || 0,
        errorCode: errorCode || 0
    };
    return jsonMessage;
}
exports.createJSONMEssage = createJSONMEssage;
