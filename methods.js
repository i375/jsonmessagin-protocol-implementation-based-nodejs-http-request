"use strict";
var Methods;
(function (Methods) {
    Methods[Methods["Login"] = 10] = "Login";
    Methods[Methods["LoginResult"] = 11] = "LoginResult";
    Methods[Methods["DoSomeAction"] = 12] = "DoSomeAction";
    Methods[Methods["DoSeomActionResult"] = 13] = "DoSeomActionResult";
})(Methods = exports.Methods || (exports.Methods = {}));
