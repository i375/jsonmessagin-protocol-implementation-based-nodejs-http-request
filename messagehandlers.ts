/// <reference path="standardsandprotocols.jsonmessaging/JSONMessaging.ts" />
import * as http from "http"

import * as jsonmessagehelpers from "./jsonmessaginghelpers"
import * as methods from "./methods"

export function login(requestMessage: JSONMessage, response: http.ServerResponse) {
    var responseMessage = jsonmessagehelpers.createJSONMEssage(methods.Methods.LoginResult, { sessionId: Math.random() }, requestMessage.messageId || 0, 0)

    response.write(JSON.stringify(responseMessage))
    response.end()
}

export function doSomeAction(requestMessage: JSONMessage, response: http.ServerResponse) {
    var responseMessage = jsonmessagehelpers.createJSONMEssage(methods.Methods.DoSeomActionResult, { someActionData: Math.random() }, requestMessage.messageId || 0, 0)

    response.write(JSON.stringify(responseMessage))
    response.end()
}