## [JSONMessaging protocol](https://bitbucket.org/9221145/standardsandprotocols.jsonmessaging) implementation, based NodeJS http (request/response) server

Request JSONMessage sample (sent as a POST request)
```
{
	"method":12, //do some action
	"params":{
		"sessionId":"1124563123d"
	},
	"messageId":"dajsdk1233as"
}
```

Response JSONMessage sample
```
{
    "method": 13, //do some action result
    "params": {
        "someActionData": 0.6721422013361007
    },
    "messageId": "dajsdk1233as",
    "errorCode": 0
}
```

Run server
```
cd project_root
node main.js
```

Compile
```
cd project_root
tsc
```
