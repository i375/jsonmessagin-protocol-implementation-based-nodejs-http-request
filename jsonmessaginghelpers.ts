/// <reference path="standardsandprotocols.jsonmessaging/JSONMessaging.ts" />

export function createJSONMEssage(methodId:number, params:JSONMessageParams | Object, messageId:number|string, errorCode:number):JSONMessage
{
    var jsonMessage = <JSONMessage>{
        method:methodId,
        params:params,
        messageId:messageId || 0,
        errorCode:errorCode || 0
    }

    return jsonMessage
}

