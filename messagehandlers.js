"use strict";
const jsonmessagehelpers = require("./jsonmessaginghelpers");
const methods = require("./methods");
function login(requestMessage, response) {
    var responseMessage = jsonmessagehelpers.createJSONMEssage(methods.Methods.LoginResult, { sessionId: Math.random() }, requestMessage.messageId || 0, 0);
    response.write(JSON.stringify(responseMessage));
    response.end();
}
exports.login = login;
function doSomeAction(requestMessage, response) {
    var responseMessage = jsonmessagehelpers.createJSONMEssage(methods.Methods.DoSeomActionResult, { someActionData: Math.random() }, requestMessage.messageId || 0, 0);
    response.write(JSON.stringify(responseMessage));
    response.end();
}
exports.doSomeAction = doSomeAction;
