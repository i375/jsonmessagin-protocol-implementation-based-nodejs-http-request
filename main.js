"use strict";
const http = require("http");
const methods = require("./methods");
const messageHandlers = require("./messagehandlers");
function buildMessageRoutes() {
    var messageRoutes = new Array();
    messageRoutes[methods.Methods.Login] = messageHandlers.login;
    messageRoutes[methods.Methods.DoSomeAction] = messageHandlers.doSomeAction;
    return messageRoutes;
}
function messageRouter(messageRoutes, data, res) {
    const requestJSON = (() => {
        try {
            return JSON.parse(data);
        }
        catch (e) {
            return null;
        }
    })();
    if (requestJSON && requestJSON.method && messageRoutes[requestJSON.method]) {
        messageRoutes[requestJSON.method](requestJSON, res);
    }
    else {
        res.setHeader("http-code", "500");
        res.end();
    }
}
function main() {
    const messageRoutes = buildMessageRoutes();
    const srv = http.createServer((req, res) => {
        req.on("data", requestDataChunkHandler);
        req.on("end", requestDataChunkEndHandler);
        var data = "";
        function requestDataChunkHandler(chunk) {
            data += chunk;
        }
        function requestDataChunkEndHandler() {
            console.log(data);
            messageRouter(messageRoutes, data, res);
        }
    });
    srv.listen(8090);
    console.log("listening on 8090");
}
main();
